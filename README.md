We invest in the development of blockchain technologies to be well-positioned today for the challenges and opportunities of the future. The number of transactions with blockchain is growing in geometric progression. 
We have set ourselves the goal of advising companies on the many possible uses of blockchain technology and developing tailor-made solutions, for example in the financial services sector, 
Also, we are actively investing in blockchain development so that this innovative technology can soon unfold its full potential. For instance, we are currently participating in various projects and are committed to integrating a large number of existing blockchain technologies into usable systems across all industries.

Website : https://blokkx.com
